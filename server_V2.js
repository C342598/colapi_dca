//Autor: Deison Colmenares
//Version: V2
//Fecha creacion: 28/05/2018
//Fecha modificado: 31/05/2018

var express = require('express'); //Importo librerias express
var bodyParser = require('body-parser'); //Utilidad para parsear info enviada en body
var requestJson = require('request-json');
var app = express();//libreria express
var port = process.env.PORT || 3000;//declaro puerto de escucha por puerto predeterminado o 3000 por default
var usersFile = require('./users2.json');
var URLbase = '/colapi/v3';

var baseMLabURL = 'https://api.mlab.com/api/1/databases/colapidb_dca/collections/';
var apiKey = 'apiKey=3Ja9mo9xNxTMRVENkIdrNQ6CNsma9Kws';

app.listen(port);
app.use(bodyParser.json());//Instrucciòn para pasar info en formato JSON a travez de body

console.log('COLAPI escucha en puerto...' +port+'...');

//***************************GET COLLECTIONS USERS********************************************
//peticion GET...
//Prueba con localhost:3000/colapi/v3/users
app.get(URLbase + '/users',
function (req, res) {
  var collect = 'user?';
  var queryStr = 'f={"_id":0}&';
//  var queryStr = 'f={"_id":0,"id":1}&';
  var rutaMLab = collect+queryStr+apiKey;
  console.log('GET ' + URLbase + '/users');
  console.log('Ruta MLab... ' + rutaMLab);
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Cliente http creado...');
  httpClient.get(rutaMLab,
    function(err, respuestaMlab, body){
      var rta = {};
      rta = !err ? body : {"msg":"Error recuperando "+collect+" de MLab..."};
      res.send(rta);
    });
});
//peticion GET con id en mlab...
//Prueba con localhost:3000/colapi/v3/users/10
app.get(URLbase + '/users' + '/:id',
function (req, res) {
  console.log('GET ' + URLbase + '/users/:id');
  var collect = 'user?';
  var iduser = req.params.id;
  var queryStr = 'f={"_id":0}&q={"id":'+iduser+'}&';
  var rutaMLab = collect+queryStr+apiKey;
  console.log('Ruta MLab... ' + rutaMLab);
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get(rutaMLab,
    function(err, respuestaMlab, body){
      var rta = {};
      rta = !err ? body[0] : {"msg":"Error recuperando "+collect+" de MLab..."};
      res.send(rta);
    });
});
//***************************DELETE COLLECTIONS USERS********************************************
//peticion DELETE con id en mlab...
//Prueba con localhost:3000/colapi/v3/users/10
app.delete(URLbase + '/users' + '/:id',
function (req, res) {
  console.log('DELETE ' + URLbase + '/users/:id');
  var collect = 'user?';
  var iduser = req.params.id;
  var queryStr = 'f={"_id":0}&q={"id":'+iduser+'}&';
  var rutaMLab = collect+queryStr+apiKey;
  console.log('Ruta MLab... ' + rutaMLab);
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.delete(rutaMLab,req.body.id,
    function(err, respuestaMlab, body){
      var rta = {};
      rta = !err ? {"msg":"Usuario "+iduser+" dado de baja en MLab..."} : {"msg":"Error recuperando "+collect+" de MLab..."};
      res.send(rta);
    });
});
//***************************POST COLLECTIONS USERS********************************************
//peticion POST...
//Prueba con localhost:3000/colapi/v3/users
app.post(URLbase + '/users',
function (req, res) {
  var collect = 'user?';
  var rutaMLab = collect+apiKey;
  console.log('POST ' + URLbase + '/users');
  console.log('Ruta MLab... ' + rutaMLab);
  var httpClient = requestJson.createClient(baseMLabURL+rutaMLab);
  console.log('Cliente http creado...');
  req.body.id = 0;
  var queryStr = 'f={"_id":0,"id":1}&s={"id":-1}&l=1&';
  var rutaMLab1 = collect+queryStr+apiKey;
//Consulta id mayor en base de usuarios...
  httpClient.get(rutaMLab1,
    function(err, respuestaMlab, body){
      var rta1 = {};
      rta1 = !err ? body[0].id : {"msg":"Error recuperando "+collect+" de MLab..."};
      console.log("rta"+rta1);
//declara consecutivo para nuevo usuario...
      req.body.id = rta1 + 1;
// Funcion POST para generar nuevo usuario....
      //res.send(rta);
      httpClient.post(rutaMLab,req.body,
        function(err, respuestaMlab, body){
          var rta = {};
          rta = !err ? body : {"msg":"Error recuperando "+collect+" de MLab..."};
          res.send(rta);
        });
    });
});
//***************************PUT COLLECTIONS USERS********************************************
//peticion PUT...
//Prueba con: localhost:3000/colapi/v3/users/10
app.put(URLbase + '/users/:id',
function (req, res) {
  var collect = 'user?';
//  var queryStr = 'f={"_id":0}&';
  var queryStr = 'q={"id":' + req.params.id + '}&';
  var rutaMLab = collect+queryStr+apiKey;
  console.log('PUT ' + URLbase + '/users');
  console.log('Ruta MLab... ' + rutaMLab);
  var httpClient = requestJson.createClient(baseMLabURL+rutaMLab);
  console.log('Cliente http creado...');
  var mod = '{"$set":' + JSON.stringify(req.body) +'}';
  httpClient.put(rutaMLab,JSON.parse(mod),
    function(err, respuestaMlab, body){
      var rta = {};
      rta = !err ? body : {"msg":"Error recuperando "+collect+" de MLab..."};
      res.send(rta);
    });
});
 //***************************GET COLLECTIONS accounts********************************************
//peticion GET...
//Prueba con: localhost:3000/colapi/v3/accounts
app.get(URLbase + '/accounts',
function (req, res) {
  var collect = 'account?';
  var queryStr = 'f={"_id":0}&';
  var rutaMLab = collect+queryStr+apiKey;
  console.log('GET ' + URLbase + '/accounts');
  console.log('Ruta MLab... ' + rutaMLab);
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Cliente http creado...');
  httpClient.get(rutaMLab,
    function(err, respuestaMlab, body){
      var rta = {};
      rta = !err ? body : {"msg":"Error recuperando "+collect+" de MLab..."};
      res.send(rta);
    });
});
//peticion GET con id en mlab...
//Prueba con: localhost:3000/colapi/v3/users/10/accounts
app.get(URLbase + '/users' + '/:id'+'/accounts',
function (req, res) {
  console.log('GET ' + URLbase + '/users' + '/:id'+'/accounts/:id');
  var collect = 'account?';
  var iduser = req.params.id;
  var queryStr = 'f={"_id":0}&q={"user_id":'+iduser+'}&';
  var rutaMLab = collect+queryStr+apiKey;
  console.log('Ruta MLab... ' + rutaMLab);
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get(rutaMLab,
    function(err, respuestaMlab, body){
      var rta = {};
      rta = !err ? body : {"msg":"Error recuperando "+collect+" de MLab..."};
      console.log(body.length);
      console.log("cuentas......");
      if(body.length==0){
        res.send({"rta":rta,"rt":body.length,"msg":"Sr Usuario, no tiene cuentas creadas..."});
      }else{
//        res.send({"msg":"Usuario:"+nombreUser+" logueado correctamente..."});
        res.send({"rta":rta,"rt":body.length,"msg":"Ok..."});
      }
//      res.send(rta);
    });
});
//***************************COLLECTIONS movements********************************************
//peticion GET...
//Prueba con. localhost:3000/colapi/v3/movements
app.get(URLbase + '/movements',
function (req, res) {
  var collect = 'movement?';
  var queryStr = 'f={"_id":0}&';
//  var queryStr = 'f={"_id":0,"id":1}&';
  var rutaMLab = collect+queryStr+apiKey;
  console.log('GET ' + URLbase + '/movements');
  console.log('Ruta MLab... ' + rutaMLab);
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Cliente http creado...');
  httpClient.get(rutaMLab,
    function(err, respuestaMlab, body){
      var rta = {};
      rta = !err ? body : {"msg":"Error recuperando "+collect+" de MLab..."};
      res.send(rta);
    });
});
//peticion GET con id en mlab...
// Prueba con localhost:3000/colapi/v3/accounts/DE30 4573 1324 2547 9767 59/movements
app.get(URLbase + '/accounts' + '/:iban' + '/movements',
function (req, res) {
  console.log('GET ' + URLbase +  '/accounts' + '/:iban' + '/movements');
  var collect = 'movement?';
  var iduser = req.params.id;
  var ibanaccounts = req.params.iban;
  var queryStr = 'f={"_id":0}&q={"iban":"'+ibanaccounts+'"}&';
  var rutaMLab = collect+queryStr+apiKey;
  console.log('Ruta MLab... ' + rutaMLab);
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get(rutaMLab,
    function(err, respuestaMlab, body){
      var rta = {};
      rta = !err ? body : {"msg":"Error recuperando "+collect+" de MLab..."};
      if(body.length==0){
        res.send({"rta":rta,"rt":body.length,"msg":"La cuenta seleccionada no tiene movimientos..."});
      }else{
//        res.send({"msg":"Usuario:"+nombreUser+" logueado correctamente..."});
        res.send({"rta":rta,"rt":body.length,"msg":"Ok..."});
      }
//      res.send(rta);
    });
});
//********************************--LOGIN CON MLAB--***********************************************
//Peticiòn POST para login con parametros en body y consulta mlab
app.post(URLbase + '/login',
function (req, res) {
  console.log('POST ' + URLbase + '/login');
  console.log(req.body.email);
  console.log(req.body.password);
//  var nom1User = req.body.first_name;
//  var nom2User = req.body.last_name;
//  var nombreUser = nom1User + ' ' +nom2User;
//  console.log(nombreUser);
  var user = req.body.email;
  var psw = req.body.password;
  req.body.logged = true;
  console.log(req.body.logged);
  var pswValido = false;
//Implementación PUT para login
  var collect = 'user?';
  var queryStr = 'q={"email":"' + user + '","password":"' + psw + '"}&';
  var rutaMLab = collect+queryStr+apiKey;
  console.log('PUT ' + URLbase + '/users');
  console.log('Ruta MLab... ' + rutaMLab);
  var httpClient = requestJson.createClient(baseMLabURL+rutaMLab);
  console.log('Cliente http creado...');
  var mod = '{"$set":' + JSON.stringify(req.body) +'}';
//Consulto cliente para saber id
  console.log('Ruta MLab... ' + rutaMLab);
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get(rutaMLab,
    function(err, respuestaMlab, body){
      var rtaid = 0;
      console.log('body en get...'+body.n);
      console.log('body 0...'+body[0]);
      if(body[0] == undefined){
        res.send({"idu":rtaid,"rt":0,"evento":"in","msg":"Usuario o Password incorrecto, Intente nuevamente..."});
      }else{
        rtaid = body[0].id;
        console.log('rta id en get...'+rtaid);
        //      res.send(rta);
        httpClient.put(rutaMLab,JSON.parse(mod),
        function(err, respuestaMlab, body){
          var rta = {};
          rta = respuestaMlab;
          var keybody = Object.keys(body);
          console.log('key...'+keybody);
          console.log('body...'+body.n);
//          console.log(body);
//          console.log(rta);
//          rta = !err ? body : {"msg":"Error recuperando "+collect+" de MLab..."};
          if(body.n==0){
            res.send({"idu":rtaid,"rt":body.n,"evento":"in","msg":"Usuario o Password incorrecto, Intente nuevamente..."});
          }else{
    //        res.send({"msg":"Usuario:"+nombreUser+" logueado correctamente..."});
            res.send({"idu":rtaid,"rt":body.n,"evento":"in","msg":"Usuario, logueado correctamente..."});
          }
        });
      }
    });
});
//********************************--LOGOUT CON MLAB--***********************************************
//Peticiòn POST para logout con parametros en body y consulta mlab
app.post(URLbase + '/logout',
function (req, res) {
  console.log('POST ' + URLbase + '/logout');
  console.log(req.body.email);
//  console.log(req.body.password);
  var user = req.body.email;
//  var psw = req.body.password;
  var pswValido = false;
//  delete req.body.logged;
  console.log('logged...'+req.body.logged);
//Implementación PUT para logout
  var collect = 'user?';
//  var queryStr = 'q={"email":"' + user + '","password":"' + psw + '"}&';
  var queryStr = 'q={"email":"' + user + '"}&';
  var rutaMLab = collect+queryStr+apiKey;
  console.log('PUT ' + URLbase + '/users');
  console.log('Ruta MLab... ' + rutaMLab);
  var httpClient = requestJson.createClient(baseMLabURL+rutaMLab);
  console.log('Cliente http creado...');
  var mod = '{"$unset":' + JSON.stringify({"logged":""}) +'}';
  httpClient.put(rutaMLab,JSON.parse(mod),
    function(err, respuestaMlab, body){
      var rta = {};
      var keybody = Object.keys(body);
      rta = !err ? body : {"msg":"Error recuperando "+collect+" de MLab..."};
      if(body.n==0){
        res.send({"rt":body.n,"evento":"out","msg":"Fallo en Logout, Intente nuevamente..."});
      }else{
        res.send({"rt":body.n,"evento":"out","msg":"Logout Ok, Hasta la próxima..."});
      }
    });
});
//********************************** PRIMER LOGIN - LOGOUT ************************************************
//Peticiòn POST para login con parametros en body
app.post(URLbase + '/login2',
function (req, res) {
  console.log('POST ' + URLbase + '/login');
  console.log(req.body.email);
  console.log(req.body.password);
  var nom1User = req.body.first_name;
  var nom2User = req.body.last_name;
  var nombreUser = nom1User + ' ' +nom2User;
  console.log(nombreUser);
  var user = req.body.email;
  var psw = req.body.password;
  var pswValido = false;
  for(us of usersFile){
    if(us.email == user){
      if(us.password == psw){
        us.logged=true;
        //console.log(us);
        writeUserDataToFile(usersFile);
        pswValido = true;
        res.send({"msg":"Usuario:"+nombreUser+" logueado correctamente..."});
      }
    }
  }
  if(!pswValido){
    res.send({"msg":"Usuario o Password incorrecto, Intente nuevamente..."});
  }
});
//Peticiòn POST para logout con parametros en body
app.post(URLbase + '/logout2',
function (req, res) {
  console.log('Entró POST ' + URLbase + '/logout');
  console.log(req.body.email);
  console.log(req.body.password);
  var user = req.body.email;
  var psw = req.body.password;
  var pswValido = false;
  for(us of usersFile){
    if(us.email == user){
      if(us.password == psw){
        delete us.logged;
        //console.log(us);
        writeUserDataToFile(usersFile);
        pswValido = true;
        res.send({"msg":"Logout Ok, Hasta la próxima..."});
      }
    }
  }
  if(!pswValido){
    res.send({"msg":"Fallo en Logout, Intente nuevamente..."});
  }
});
//Deja en firme en archivo
function writeUserDataToFile(data) {
 var fs = require('fs');
 var jsonUserData = JSON.stringify(data);

 fs.writeFile("./users2.json", jsonUserData, "utf8",
  function(err) { //función manejadora para gestionar errores de escritura
    if(err) {
      console.log(err);
    } else {
      console.log("Datos escritos en 'Users2.json'.");
    }
  })
}
