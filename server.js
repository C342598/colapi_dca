var express = require('express'); //Importo librerias express
var bodyParser = require('body-parser'); //Utilidad para parsear info enviada en body
var app = express();
var port = process.env.PORT || 3000;//declaro puerto de escucha por puerto predeterminado o 3000 por default
var usersFile = require('./users.json');
var URLbase = '/colapi/v1';
app.listen(port);
app.use(bodyParser.json());//Instrucciòn para pasar info en formato JSON a travez de body

console.log('COLAPI escucha en puerto...' +port+'...');

//peticion GET con query string parametros por URL...
app.get(URLbase + '/usersqv',
function (req, res) {
  console.log('GET con query varias salidas' + URLbase + '/usersqv');
  console.log(req.query.id); //localhost:3000/colapi/v1/usersq?id=1&country=Colombia
  console.log(req.query.country); //localhost:3000/colapi/v1/usersq?id=1&country=Colombia
  var iduser = req.query.id;
  var qcountry = re.query.country;
//  res.send(usersFile[iduser-1]);
  res.send(usersFile[qcountry]);
//  res.send({'msg':'Ok query!'}); //muestra parametros en formato json
//  res.send(iduser); //muestra valor de parametros separados por comma
//  console.log(req.params);
//  console.log(req.headers);
//  res.send({'msg':'Nuevo Hola Mundo!'});
});

//peticion GET con query string parametros por URL...
//Query string se usa para obtener resultados
app.get(URLbase + '/usersq',
function (req, res) {
  console.log('GET con query' + URLbase + '/usersq');
  console.log(req.query.id); //localhost:3000/colapi/v1/usersq?id=1&country=Colombia
  console.log(req.query.country); //localhost:3000/colapi/v1/usersq?id=1&country=Colombia
  var iduser = req.query.id;
  res.send(usersFile[iduser-1]);
//  res.send(usersFile[qcountry]);
//  res.send({'msg':'Ok query!'}); //muestra parametros en formato json
//  res.send(iduser); //muestra valor de parametros separados por comma
//  console.log(req.params);
//  console.log(req.headers);
//  res.send({'msg':'Nuevo Hola Mundo!'});
});

app.get(URLbase + '/users',
function (req, res) {
  console.log('GET ' + URLbase + '/users' + '');
  res.sendfile('./users.json');
//  console.log(req.params);
//  console.log(req.headers);
//  res.send({'msg':'Nuevo Hola Mundo!'});
});
//peticion GET con 1 parametro...
app.get(URLbase + '/users' + '/:id',
function (req, res) {
  console.log('GET ' + URLbase + '/users' + '/users/:id');
  var iduser = req.params.id;
  res.send(usersFile[iduser-1]);
//  console.log(req.params);
//  console.log(req.headers);
//  res.send({'msg':'Nuevo Hola Mundo!'});
});
//peticion GET con 2 parametros...
app.get(URLbase + '/users' + '/:id/:otro',
function (req, res) {
  console.log('GET ' + URLbase + '/users' + '/users/:id/:otro');
  var iduser = req.params.id + ',' + req.params.otro;
  res.send(req.params); //muestra parametros en formato json
//  res.send(iduser); //muestra valor de parametros separados por comma
//  console.log(req.params);
//  console.log(req.headers);
//  res.send({'msg':'Nuevo Hola Mundo!'});
});
//Peticiòn POST
//permite añadir un nuevo recurso
app.post(URLbase + '/userssin',
function (req, res) {
  console.log('POST ok...');
  res.sendFile('users.json');
});

//Peticiòn POST
app.post(URLbase + '/usersj',
function (req, res) {
  console.log(req.body);
  console.log('POST ok...');
  console.log(req.body.first_name);
  var jsonID = {};
  var newID = usersFile.length + 1;
  jsonID = req.body;
  jsonID.id = newID;
  console.log(newID);
  console.log(jsonID);
  usersFile.push(jsonID);
  console.log(usersFile);
  res.send({"msg" : "Alta user ok...", jsonID});
});


//Peticiòn PUT
app.put(URLbase + '/usersp/:id',
function (req, res) {
  var buscarId = req.params.id;
  var updateUsesr = req.body;
  console.log('Inicio put...'+buscarId);
  console.log(updateUsesr);
  console.log(buscarId);
  console.log(usersFile.length);
  var encontro = 0;
  for (var user=1; user<usersFile.length;user++){
      if(usersFile[user].id == buscarId){
        console.log('Encontrado...' + usersFile[user].id);
        encontro ++;
      }
  }
  if (encontro == 0){
    console.log('Usuario con id:' + buscarId + ' no existe!!!');
  }
  res.send({"msg" : "Modificado ok..."});
});

//Peticiòn POST
app.post(URLbase + '/usersP',
function (req, res) {
  console.log('POST ok...');
  console.log('POST ' + URLbase + '/usersP' + '');
  res.sendFile('users.json',{root: __dirname});
//  res.sendFile('users.json');//Deprecates
//  res.send({'msg':'petición post ok...'});
});

app.post(URLbase + '/users',
function (req, res) {
  console.log('POST ' + URLbase + '/users' + '');
  res.send({'msg':'post ok...'});
});

app.put(URLbase + '/users' + '',
function (req, res) {
  console.log('PUT ' + URLbase + '/users' + '');
  res.send({'msg':'put ok...'});
});

app.delete(URLbase + '/users' + '',
function (req, res) {
  console.log('DELETE ' + URLbase + '/users' + '');
  res.send({'msg':'DELETE ok...'});
});

// PUT
app.put(URLbase + '/users/:id',
  function(req, res){
    console.log("PUT /colapi/v2/users/:id");
    var idBuscar = req.params.id;
    var updateUser = req.body;
    for(i = 0; i < usersFile.length; i++) {
      console.log(usersFile[i].id);
      if(usersFile[i].id == idBuscar) {
        res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
        i=usersFile.length;
      }
    }
    res.send({"msg" : "Usuario no encontrado.", updateUser});
  });
  // PUT
  app.delete(URLbase + '/users/:id',
    function(req, res){
      console.log("DELETE /colapi/v2/users/:id");
      var idBuscar = req.params.id;
      var deleteUser = req.body;
      for(i = 0; i < usersFile.length; i++) {
        console.log(usersFile[i].id);
        if(usersFile[i].id == idBuscar) {
          console.log(usersFile[i].id);
          usersFile.splice(idBuscar,1);
          res.send(204);
          //res.send({"msg" : "Usuario eliminado correctamente.", deleteUser});
          i=usersFile.length;
        }
      }
      res.send({"msg" : "Usuario no encontrado.", deleteUser});
    });

/*
app.listen(3000, function () {
  console.log('Escuchando... En ellll puerto: 3000!');
});
*/
