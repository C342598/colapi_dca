var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server_V2');
// Declaro should que me indica lo que "podria" hacer con mi escucha...
var should = chai.should();
//Configurar chai-herramienta para definir pruebas... con modulo HTTP
chai.use(chaiHttp);
//Defino suite de pruebas donde () => me dice "en caso de nada haga lo que hay en la function"

describe('Pruebas Colombia', () => {
  it('BBVA funciona', (done) => {
    chai.request('http://www.bbva.com')
      .get('/')
        .end((err, res) => {
//        console.log(res);
          res.should.have.status(200);
        done();
      })
  });
  it('BBVA en colapi/users', (done) => {
    chai.request('http://localhost:3000')
      .get('/colapi/v3/users')
        .end((err, res) => {
//        console.log('cont log...'+res);
          res.should.have.status(200);
        done();
      })
  });
  it('BBVA devuelve array en colapi/users', (done) => {
    chai.request('http://localhost:3000')
      .get('/colapi/v3/users')
        .end((err, res) => {
          console.log(res.body);
          res.body.should.be.a('array');
        done();
      })
  });
  it('BBVA valida longitud en colapi/users', (done) => {
    chai.request('http://localhost:3000')
      .get('/colapi/v3/users')
        .end((err, res) => {
//          console.log(res.body);
          res.body.length.should.be.gte(1);
        done();
      })
  });
  it('BBVA Valida 1er elemento en colapi/users', (done) => {
    chai.request('http://localhost:3000')
      .get('/colapi/v3/users')
        .end((err, res) => {
          console.log(res.body[0]);
          res.body[0].should.have.property('first_name');
          res.body[0].should.have.property('last_name');
        done();
      })
  });
  it('BBVA POST en colapi/users', (done) => {
    chai.request('http://localhost:3000')
      .post('/colapi/v3/users')
      .send({"first_name":"Christopher","last_name":"Colmenares","email":"ccj@gmail.com","password":"11111111"})
        .end((err, res, body) => {
          console.log(res.body[0]);
        done();
      })
  });
});
